<?php
/**
 * @author Craft-Soft Team
 * @package CS:Bans
 * @version 1.0 beta
 * @copyright (C)2013 Craft-Soft.ru.  Все права защищены.
 * @link http://craft-soft.ru/
 * @license http://creativecommons.org/licenses/by-nc-sa/4.0/deed.ru  «Attribution-NonCommercial-ShareAlike»
 */

/*
 -- CREATE FIELD "expired" --------------------------------------
ALTER TABLE `users_prefix` ADD COLUMN `created` INT( 255 ) UNSIGNED NULL AFTER `prefix`;
ALTER TABLE `users_prefix` ADD COLUMN `expired` Int( 255 ) UNSIGNED NULL;
UPDATE `users_prefix` SET `created` = 0, `expired` = 0
ALTER TABLE `users_prefix` MODIFY `created` Int( 255 ) UNSIGNED NULL DEFAULT '0';
ALTER TABLE `users_prefix` MODIFY `expired` Int( 255 ) UNSIGNED NULL DEFAULT '0';
ALTER TABLE `users_prefix` ADD COLUMN `active` TINYINT( 1 ) UNSIGNED NULL DEFAULT '0' AFTER `prefix`;
-- -------------------------------------------------------------
 */

/**
 * Модель для таблицы "{{reasons}}".
 *
 * Доступные поля таблицы '{{reasons}}':
 * @property integer $id ID причины
 * @property string $nickname Причина
 * @property string $prefix Срок бана
 * @property boolean $active
 * @property int $created
 * @property int $expired
 */
class Prefixes extends CActiveRecord
{
	public $days;
	public $change;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'users_prefix';
	}

	public function rules()
	{
		return array(
			//array('static_bantime', 'numerical', 'integerOnly'=>true),
			array('prefix', 'length', 'max'=>100),
			array('nickname, prefix', 'required'),
			array('days, change', 'safe'),
			array('days', 'numerical', 'min' => 0, 'allowEmpty' => true),
			array('active, change', 'boolean', 'allowEmpty' => true),
			array('id, nickname, prefix', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'admin' => array(
				self::BELONGS_TO,
				'Prefixes',
				array('nickname'=>'nickname')
			),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nickname' => 'Нікнейм',
			'prefix' => 'Префікс',
			'active' => 'Активний',
			'created' => 'Дата створення',
			'expired' => 'Дата заевршення',
			'days' => 'Дата заевршення',
			'change' => 'Змінити термін',
		);
	}

	protected function beforeSave()
	{
		if($this->isNewRecord)
		{
			$this->created = time();
			$this->expired = $this->days != 0 ? ($this->days * 86400) + time() : 0;
		}
		elseif($this->change)
		{
			$this->expired = $this->days != 0 ? ($this->days * 86400) + time() : 0;
		}

		return parent::beforeSave();
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nickname',$this->nickname,true);
		$criteria->compare('prefix',$this->prefix);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function getList($addban = TRUE) {
		return self::model()->findAll();
	}
	
	public function afterSave() {
		if($this->isNewRecord)
			Syslog::add(Logs::LOG_ADDED, 'Добавлена причина банов <strong>' . $this->nickname . '</strong>');
		else
			Syslog::add(Logs::LOG_EDITED, 'Изменена причина банов <strong>' . $this->nickname . '</strong>');
		return parent::afterSave();
	}
	
	public function afterDelete() {
		Syslog::add(Logs::LOG_DELETED, 'Удалена причина банов <strong>' . $this->nickname . '</strong>');
		return parent::afterDelete();
	}
}