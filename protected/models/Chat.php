<?php

class Chat extends CActiveRecord
{
	/**
	 * @param string $className
	 *
	 * @return Chat
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{chat}}';
	}

	public function rules()
	{
		return array();
	}

	public function relations()
	{
		return array();
	}

	public function attributeLabels()
	{
		return array();
	}

	public function search($date)
	{
		$query = 'SELECT * FROM `'.$this->tableName().'` WHERE DATE_FORMAT(FROM_UNIXTIME(send_date), "%d-%m-%Y") = :send_date order by send_date ASC';

		return $this->getDbConnection()->createCommand($query)->queryAll(true, array(
			':send_date' => $date
		));
	}
}