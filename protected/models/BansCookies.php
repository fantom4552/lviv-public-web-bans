<?php
/**
 * @author Craft-Soft Team
 * @package CS:Bans
 * @version 1.0 beta
 * @copyright (C)2013 Craft-Soft.ru.  Все права защищены.
 * @link http://craft-soft.ru/
 * @license http://creativecommons.org/licenses/by-nc-sa/4.0/deed.ru  «Attribution-NonCommercial-ShareAlike»
 */

/**
 * Модель для таблицы "{{bans}}".
 * Доступные поля таблицы '{{bans}}':
 * @property integer $id ID
 * @property string $hash Code
 *
 * The followings are the available model relations:
 * @property integer $bansCount
 * @property Bans[] $bans
 */
class BansCookies extends CActiveRecord
{
	/**
	 * @param string $className
	 *
	 * @return BansCookies
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{bans_cookies}}';
	}

	public function rules()
	{
		return array(
			//
		);
	}

	public function relations()
	{
		return array(
			'bansCount' => array(
				self::STAT,
				'Bans',
				'cookie_id',
				'defaultValue' => 0,
				'condition' => 't.`expired` = 0',
			),
			'bans' => array(
                self::HAS_MANY,
                'Bans',
                'cookie_id',
				'order' => 'bans.bid DESC',
				'condition' => 'bans.`expired` = 0',
            ),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id'    => 'Id',
			'hash'  => 'Code',
		);
	}

	/**
	 * Настройки поиска
	 * @return \CActiveDataProvider
	 */
	/*public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('bid',$this->bid);
		$criteria->addSearchCondition('player_ip',$this->player_ip);
		$criteria->addSearchCondition('player_id',$this->player_id);
		$criteria->addSearchCondition('player_nick',$this->player_nick);
		$criteria->compare('admin_ip',$this->admin_ip,true);
		$criteria->compare('admin_id',$this->admin_id,true);
		if ($this->admin_nick) {
            $criteria->compare('admin_nick', $this->admin_nick, true);
        }
        $criteria->compare('ban_type',$this->ban_type,true);
		$criteria->addSearchCondition('ban_reason',$this->ban_reason);
		$criteria->compare('cs_ban_reason',$this->cs_ban_reason,true);
		if ($this->ban_created) {
            $start = strtotime("{$this->ban_created} 00:00:00");
            $end = strtotime("{$this->ban_created} 23:59:59");
            $criteria->addBetweenCondition('ban_created', $start, $end);
        }
        $criteria->compare('ban_length',$this->ban_length);
		$criteria->compare('server_ip',$this->server_ip,true);
		$criteria->compare('server_name',$this->server_name,true);
		$criteria->compare('ban_kicks',$this->ban_kicks);
		$criteria->compare('expired',$this->expired);
		$criteria->compare('imported',$this->imported);

		$criteria->order = '`bid` DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => Yii::app()->config->bans_per_page
			)
		));
	}*/
}
