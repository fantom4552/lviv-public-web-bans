<?php

/**
 * Class ServerRanks
 * @property int $id
 * @property string $name
 * @property int $exp
 */
class ServerRanks extends CActiveRecord
{
	public $rank;

	private $levels = array(0, 15, 30, 60, 100, 180, 350, 750, 999, 1500, 2200, 2800, 3200, 3900, 4500, 5000, 5500, 6000, 7000, 8000, 12000, 15000, 20000, 22000, 27000, 35000, 40000, 50000, 77777, 999999, 999999999);
	
	private $ranks = array(
		0 => 'Дух',
		1 => 'Новобранець',
		2 => 'Школота',
		3 => 'Старшокласник',
		4 => 'Випускник',
		5 => 'Абітура',
		6 => 'Студік',
		7 => 'Прошарений',
		8 => 'Дипломник',
		9 => 'Ст.Випускник',
		10 => 'Стажер',
		11 => 'Ст.Стажер',
		12 => 'Робочий',
		13 => 'Ст.Робочий',
		14 => 'Партнер',
		15 => 'Мажор',
		16 => 'Олігарх',
		17 => 'Задрот',
		18 => 'Тру-Задрот',
		19 => 'Космо-ЗАДР',
		20 => 'Альфа-самець',
		21 => 'Своя людина',
		22 => 'Житель Сервера',
		23 => 'Кіллер',
		24 => 'Мясник',
		25 => 'Рембо',
		26 => 'Винищувач',
		27 => 'Unreal Killer',
		28 => 'Шеф',
		29 => 'Шеф',
	);
	/**
	 * @param string $className
	 *
	 * @return ServerRanks
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'server_ranks';
	}

	public function rules()
	{
		return array(
			array('id, name, exp, rank', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array();
	}

	public function attributeLabels()
	{
		return array(
			'id'    => 'ID',
			'name'  => 'Нікнейм',
			'exp'   => 'Досвід',
			'rank'  => 'Титул',
		);
	}

	public function getRank()
	{
		$level = 0;
		if($this->exp >= 15 && $this->exp < 77777)
		{
			while($this->exp >= $this->levels[$level])
			{
				$level++;
			}
			$level--;
		}
		elseif($this->exp >= 77777)
		{
			$level = 29;
		}

		return $this->ranks[$level];
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->addSearchCondition('name',$this->name);
		$criteria->compare('exp',$this->exp);

		$criteria->order = '`exp` DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => Yii::app()->config->bans_per_page
			)
		));
	}
}