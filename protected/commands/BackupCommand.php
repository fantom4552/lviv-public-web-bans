<?php
class BackupCommand extends CConsoleCommand
{
	public function run($args)
	{
		Yii::import('ext.dumpDB.dumpDB');
		$dumper = new dumpDB();
		$bk_file = ROOTPATH.'/include/backups/backup-'.date('Ymd_His').'.sql';
		$fh = fopen($bk_file, 'w') or die("can't open file");
		fwrite($fh, $dumper->getDump(FALSE));
		fclose($fh);
	}
}