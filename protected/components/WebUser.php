<?php
/**
 * Created by esterio.
 * User: trepa
 * Date: 5/6/15
 * Time: 3:47 PM
 */

class WebUser extends CWebUser
{
	protected function afterLogin($fromCookie)
	{
		$this->setState('isAdmin', true);
		Yii::app()->controller->redirect(array('/admin/index'));
	}
}