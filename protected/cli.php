<?php
date_default_timezone_set('Europe/Kiev');

define('ROOTPATH', dirname(__DIR__));

// Дебаг
defined('YII_DEBUG') or define('YII_DEBUG', true);
// Кол-во стак трейсов ошибок
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

// Ядро
require_once(ROOTPATH . '/include/yii/framework/yii.php');

// Создаем приложение
Yii::createConsoleApplication(ROOTPATH . '/protected/config/cli.php')->run();