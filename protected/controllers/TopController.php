<?php

class TopController extends Controller
{
	public $layout='//layouts/column1';

	public function actionIndex()
	{
		$model=new ServerRanks('search');
		$model->unsetAttributes();
		if (isset($_POST['ServerRanks'])) {
			$model->attributes = $_POST['ServerRanks'];
		}

		$this->render('index', array(
			'dataProvider'=>$model->search(),
			'model'=>$model,
		));
	}
}
