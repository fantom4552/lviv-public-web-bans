<?php
/**
 * Контроллер админцентра
 */

/**
 * @author Craft-Soft Team
 * @package CS:Bans
 * @version 1.0 beta
 * @copyright (C)2013 Craft-Soft.ru.  Все права защищены.
 * @link http://craft-soft.ru/
 * @license http://creativecommons.org/licenses/by-nc-sa/4.0/deed.ru  «Attribution-NonCommercial-ShareAlike»
 */

class PrefixesController extends Controller
{

	public $layout='//layouts/column2';

    public function filters()
    {
		return array(
			'accessControl',
		);
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		if(!Webadmins::checkAccess('prefixes_edit'))
			throw new CHttpException(403, 'У Вас недостаточно прав');

		$model=new Prefixes();
		$model->active = true;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Prefixes']))
		{
			$model->attributes=$_POST['Prefixes'];

			if($model->save())
				$this->redirect(array('/admin/prefixes'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionUpdate($id)
	{
		if(!Webadmins::checkAccess('prefixes_edit'))
			throw new CHttpException(403, 'У Вас недостаточно прав');

		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Prefixes']))
		{
			$model->attributes=$_POST['Prefixes'];
			if($model->save())
				$this->redirect(array('/admin/prefixes'));
		}

		$model->change = false;

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		if(!Webadmins::checkAccess('prefixes_edit'))
			throw new CHttpException(403, 'У Вас недостаточно прав');

		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 *
	 * @throws CHttpException
	 *
	 * @return Prefixes
	 */
	public function loadModel($id)
	{
		$model=Prefixes::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='prefixes-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}