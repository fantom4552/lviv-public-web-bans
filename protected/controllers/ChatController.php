<?php

class ChatController extends Controller
{

	public $layout='//layouts/main';

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function actionIndex()
	{
		if(Yii::app()->user->isGuest)
			throw new CHttpException(403, "У Вас недостаточно прав");

		$data = Chat::model()->search(Yii::app()->request->getParam('date', date('d-m-Y')));

		$this->render('index', array(
			'data' => $data
		));
	}
}
