<?php
/**
 * Контррллер банов
 */

/**
 * @author Craft-Soft Team
 * @package CS:Bans
 * @version 1.0 beta
 * @copyright (C)2013 Craft-Soft.ru.  Все права защищены.
 * @link http://craft-soft.ru/
 * @license http://creativecommons.org/licenses/by-nc-sa/4.0/deed.ru  «Attribution-NonCommercial-ShareAlike»
 */
class MotdController extends Controller
{
	public $layout = false;

	public function actionIndex($uid)
	{
		$hash = Yii::app()->request->cookies['test'];
		
		/** @var BansCookies $cookie */
		$cookie = BansCookies::model()->findByAttributes(array(
			'hash' => $hash->value,
		));

		if($cookie && $cookie->bansCount > 0)
		{
			/** @var Bans $ban */
			$ban = $cookie->bans[0];

			$server = Serverinfo::model()->findByAttributes(array(
				'address' => $ban->server_ip,
			));

			$command = 'amx_ban_cookie '.((int)$uid).' '.$ban->bid;

			$server->RconCommand(CHtml::encode($command));
		}
	}

	public function actionBan($sid, $adm = 0, $lang = 'ru')
	{
		$this->layout = FALSE;

		$sid = (int)SubStr( $sid, 1 );

		/** @var Bans $model */
		$model = Bans::model()->findByPk($sid);
		if ($model === null) {
			Yii::app()->end('Error!');
		}

		$hash = Yii::app()->request->cookies['test'];
		$cookie = NULL;
		$new_cookie = false;

		if($hash)
		{
			$cookie =  BansCookies::model()->findByAttributes(array(
				'hash' => $hash->value,
			));
		}

		if(!$cookie)
		{
			$cookie = new BansCookies();
			$cookie->hash= md5(uniqid(time()));
			$cookie->save();
			$new_cookie = true;
		}

		$model->cookie_id = $cookie->id;
		$model->save(false, array('cookie_id'));


		if($new_cookie)
		{
			Yii::app()->request->cookies['test'] = new CHttpCookie('test', $cookie->hash, array(
				'path' => '/',
				'expire' => strtotime('+2 years', time()),
				'httpOnly' => true,
			));
		}

		$this->render('motd', array(
			'model'=>$model,
			'show_admin' => $adm == 1 ? TRUE : FALSE
		));
	}
}