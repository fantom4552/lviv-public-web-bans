<?php
/**
 * @var TopController $this
 * @var ServerRanks $model
 * @var CActiveDataProvider $dataProvider
 */

$page = 'Топ';
$this->pageTitle = Yii::app()->name . ' - ' . $page;

$this->breadcrumbs=array(
	$page,
);
?>
<style>
	.form-search  input {
		margin-left: 10px;
	}
</style>

<?php

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->request->getRequestUri(),
	'method'=>'post',
	'htmlOptions' => array(
		'class' => 'form-search'
	),
));
?>
	<?php echo $form->textFieldRow($model, 'name', array(
		'maxlength'=>100,
		'class' => 'search-query',
	)); ?>
	<input type="submit" class="btn" value="Пошук">
<?php $this->endWidget(); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
	'id'=>'top-grid',
	'dataProvider'=>isset($_GET['ServerRanks']) ? $model->search() : $dataProvider,
	//'template' => '{items} {pager}',
	'summaryText' => 'Показано з {start} по {end} гравців з {count}. Сторінка {page} з {pages}',
	'htmlOptions' => array(
		'style' => 'width: 100%'
	),
	'pager' => array(
		'class'=>'bootstrap.widgets.TbPager',
		'displayFirstAndLast' => true,
	),
	'columns'=>array(
		array(
			'header' => 'Нікнейм',
			'type' => 'raw',
			'value' => function($data){
				return CHtml::encode($data->name);
			},
		),
		array(
			'header' => 'Досвід',
			'value' => function($data){
				return $data->exp;
			},
		),
		array(
			'header' => 'Титул',
			'value' => function($data){
				return $data->getRank();
			},
		),
	),
)); ?>