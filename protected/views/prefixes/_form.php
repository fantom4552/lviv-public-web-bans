<?php
/**
 * Форма добавления/редактирования причин банов
 */

/**
 * @author Craft-Soft Team
 * @package CS:Bans
 * @version 1.0 beta
 * @copyright (C)2013 Craft-Soft.ru.  Все права защищены.
 * @link http://craft-soft.ru/
 * @license http://creativecommons.org/licenses/by-nc-sa/4.0/deed.ru  «Attribution-NonCommercial-ShareAlike»
 */

Yii::app()->clientScript->registerScript('adminactions', '
	var days = $("#Prefixes_days");
	var change = $("#Prefixes_change");
	var forever = $("#forever");

	forever.click(function() {
		if($(this).prop("checked")) {
			days.val("0");
			days.attr("readonly", "readonly");
		} else {
			days.removeAttr("readonly");
			days.val("30");
		}
	});

	change.click(function() {
		if($(this).prop("checked")) {
			forever.removeAttr("readonly");
			days.removeAttr("readonly");
			days.val("0");
		} else {
			days.val("0");
			days.attr("readonly", "readonly");
			forever.attr("readonly", "readonly");
		}
	});
');

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'prefixes-form',
	'enableAjaxValidation'=>TRUE,
));
?>

	<p class="note">Поля, отмеченные <span class="required">*</span> обязательны к заполнению.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nickname',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'prefix',array('class'=>'span5')); ?>

	<div class="row-fluid">
		<label for="Prefixes_active" class="checkbox">
			<?php
			echo $form->checkBox($model, 'active');
			echo $model->getAttributeLabel('active');
			?>
		</label>
	</div>

	<?php if($model->isNewRecord)
	{
		echo $form->textFieldRow(
			$model,
			'days',
			array(
				'class' => 'span6',
				'value' => '0',
				'readonly' => 'readonly',
				'append' => '<label>' . CHtml::checkBox('', true, array('id' => 'forever')) . ' навсегда</label>'
			)
		);
	}
	else
	{
		//if($model->expired != 0)
		{
			?>
			<div class="row-fluid">
				<label for="Prefixes_change" class="checkbox">
					<?php
					echo $form->checkBox($model, 'change');
					echo $model->getAttributeLabel('change');
					?>
				</label>
				<?php

				echo $form->textFieldRow(
					$model,
					'days',
					array(
						'class' => 'span6',
						'value' => '0',
						'readonly' => 'readonly',
						'append' => '<label>' . CHtml::checkBox('', true, array('id' => 'forever', 'readonly' => 'readonly')) . ' навсегда</label>'
					)
				);
				?>
			</div>
			<?php
		}
	}
	?>


	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить',
		)); ?>
		<?php echo CHtml::link(
				'Отмена',
				Yii::app()->createUrl('/admin/prefixes'),
				array(
					'class' => 'btn btn-danger'
				)
			);
		?>
	</div>

<?php $this->endWidget(); ?>
