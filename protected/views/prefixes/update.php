<?php
/**
 * Вьюшка редактирования причины бана
 */

/**
 * @author Craft-Soft Team
 * @package CS:Bans
 * @version 1.0 beta
 * @copyright (C)2013 Craft-Soft.ru.  Все права защищены.
 * @link http://craft-soft.ru/
 * @license http://creativecommons.org/licenses/by-nc-sa/4.0/deed.ru  «Attribution-NonCommercial-ShareAlike»
 */

$this->pageTitle = Yii::app()->name .' :: Адмінцентр - Префікси';

$this->breadcrumbs=array(
	'Адмінцентр'=>array('/admin/index'),
	'Префікси' => array('/admin/prefixes'),
	'Редактировать префікс ' . $model->nickname
);

$this->renderPartial('/admin/mainmenu', array('active' =>'advanced', 'activebtn' => 'prefixes'));

?>

<h2>Редактировать префікс "<?php echo $model->nickname; ?>"</h2>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>