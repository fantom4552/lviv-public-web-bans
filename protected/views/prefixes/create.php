<?php
/**
 * Вьюшка добавления причины бана
 */

/**
 * @author Craft-Soft Team
 * @package CS:Bans
 * @version 1.0 beta
 * @copyright (C)2013 Craft-Soft.ru.  Все права защищены.
 * @link http://craft-soft.ru/
 * @license http://creativecommons.org/licenses/by-nc-sa/4.0/deed.ru  «Attribution-NonCommercial-ShareAlike»
 */

$page = 'Префікси :: Добавити префікс';
$this->pageTitle = Yii::app()->name . ' - ' . $page;

$this->breadcrumbs=array(
	'Адмінцентр'=>array('/admin/index'),
	'Префікси' => array('/admin/prefixes'),
	'Добавити префікс'
);

$this->renderPartial('/admin/mainmenu', array('active' =>'advanced', 'activebtn' => 'prefixes'));

?>

<h2>Добавити префікс</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>