<?php
/**
 * @var ChatController $this
 * @var array $data
 */
?>

<?php foreach($data as $row): ?>
	<?php echo date('d.m.y H:i:s', $row['send_date']); ?> &lt;<?php echo $row['steam_id']; ?>&gt;&lt;<?php echo $row['ip']; ?>&gt; <?php echo $row['nickname']; ?> - <?php echo $row['message']; ?><br>
<?php endforeach; ?>