<?php
/**
 * Вьюшка главной страницы админов серверов
 */

/**
 * @author Craft-Soft Team
 * @package CS:Bans
 * @version 1.0 beta
 * @copyright (C)2013 Craft-Soft.ru.  Все права защищены.
 * @link http://craft-soft.ru/
 * @license http://creativecommons.org/licenses/by-nc-sa/4.0/deed.ru  «Attribution-NonCommercial-ShareAlike»
 */

$page = 'Админістратори';

$this->pageTitle = Yii::app()->name . ' - ' . $page;

$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links'=>array($page),
));
?>

<h2><?php echo $page; ?></h2>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'dataProvider'=>$admins,
	'type'=>'striped bordered condensed',
	'id' => 'admins-grid',
	//'template' => '{items} {pager}',
	'summaryText' => 'Показано з {start} по {end} адмінів з {count}. Сторінка {page} з {pages}',
	'enableSorting' => false,
	'rowHtmlOptionsExpression'=>'array(
		"id" => "admin_$data->id",
		"class" => "admintr"
	)',
	'pager' => array(
		'class'=>'bootstrap.widgets.TbPager',
		'displayFirstAndLast' => true,
	),
	'columns'=>array(
		array(
			'name' => 'nickname',
			'type' => 'raw',
			'value' => function($data){
				$result = $data->nickname;
				if(!empty($data->prefix))
				{
					$result .= '&nbsp;[&nbsp;<span class="adminPrefix">'.$data->prefix->prefix.'</span>&nbsp;]';
				}
				elseif(stripos($data->access, 'd') !== false)
				{
					$result .= '&nbsp;[&nbsp;<span class="adminPrefix">Адмін</span>&nbsp;]';
				}
				elseif(stripos($data->access, 't') !== false)
				{
					$result .= '&nbsp;[&nbsp;<span class="adminPrefix">VIP</span>&nbsp;]';
				}

				return $result;
			},
			'htmlOptions' => array(
				'style' => 'width: 340px;'
			)
		),
		array(
			'name' => 'created',
			'value' => function($data){
				return date("d.m.Y - H:i:s", $data->created);
			},
			'htmlOptions' => array(
				'style' => 'width: 170px; text-align: center'
			)
		),
	),
)); ?>