<?php
/**
 * Вьюшка причин банов
 */

/**
 * @author Craft-Soft Team
 * @package CS:Bans
 * @version 1.0 beta
 * @copyright (C)2013 Craft-Soft.ru.  Все права защищены.
 * @link http://craft-soft.ru/
 * @license http://creativecommons.org/licenses/by-nc-sa/4.0/deed.ru  «Attribution-NonCommercial-ShareAlike»
 */

$page = 'Префікси';
$this->pageTitle = Yii::app()->name . ' - ' . $page;

$this->breadcrumbs=array(
	'Адмінцентр'=>array('/admin/index'),
	$page
);

$this->renderPartial('/admin/mainmenu', array('active' =>'advanced', 'activebtn' => 'prefixes'));

$this->menu=array(
	array('label'=>'Добавити префікс','url'=>array('/prefixes/create')),
);
?>

<h2>Управління префіксами</h2>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'type' => 'bordered condensed striped',
	'id'=>'prefixes-grid',
	'dataProvider'=>$prefixes,
	'template' => '{pager} {items}',
	//'summaryText' => 'Показано с {start} по {end} причин из {count}. Страница {page} из {pages}',
	'pager' => array(
		'class'=>'bootstrap.widgets.TbPager',
		'displayFirstAndLast' => true,
	),
	'enableSorting' => FALSE,
	'columns'=>array(
		'nickname',
		'prefix',
		array(
			'name' => 'created',
			'value' => 'date("d.m.Y - H:i:s",$data->created)',
			'htmlOptions' => array(
				'style' => 'width: 170px; text-align: center'
			)
		),

		array(
			'name' => 'expired',
			'type' => 'raw',
			'value' => '$data->expired == 0 ? "<i>Никогда</i>" : date("d.m.Y - H:i:s",$data->expired)',
			'htmlOptions' => array(
				'style' => 'width: 170px; text-align: center'
			)
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} {delete}',
			'htmlOptions' => array(
				'style' => 'width: 25px;'
			),
			'buttons' => array(
				'update' => array(
					'url' => 'Yii::app()->createUrl("/prefixes/update", array("id" => $data->id))'
				),
				'delete' => array(
					'url' => 'Yii::app()->createUrl("/prefixes/delete", array("id" => $data->id))'
				)
			)
		),
	),
)); ?>