<?php
/**
 * Вьюшка причин банов
 */

/**
 * @author Craft-Soft Team
 * @package CS:Bans
 * @version 1.0 beta
 * @copyright (C)2013 Craft-Soft.ru.  Все права защищены.
 * @link http://craft-soft.ru/
 * @license http://creativecommons.org/licenses/by-nc-sa/4.0/deed.ru  «Attribution-NonCommercial-ShareAlike»
 */

Yii::app()->clientScript->registerScript('command', '
	var commandURL = "'.Yii::app()->createUrl('/serverinfo/sendcommand', array('id' => 'ID')).'";
	$("#sendcommand").click(function() {
		var ID = $("#serverlist").val();
		var command = $("#commandlist").val();

		if(!ID)
			return alert("Виберіть сервер");

		if(!command)
			return alert("Введіть команду");

		$.post(
			commandURL.replace("ID", ID),
			{
				"'.Yii::app()->request->csrfTokenName.'": "'.Yii::app()->request->csrfToken.'",
				"command": command
			},
			function(data) {$("#output").html(data);}
		);
		return false;
	})
');

$page = 'RCON';
$this->pageTitle = Yii::app()->name . ' - ' . $page;

$this->breadcrumbs=array(
	'Адмінцентр'=>array('/admin/index'),
	$page
);

$this->renderPartial('/admin/mainmenu', array('active' =>'advanced', 'activebtn' => 'rcon'));
?>

<?php //if(!$model->isNewrecord && $model->rcon): ?>
	<table class="table table-bordered">
		<thead>
		<tr>
			<th>
				Відправити RCON команду на сервер
			</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>
				<?php echo CHtml::dropDownList('', '', Serverinfo::getAllServers(false, true), array('id' => 'serverlist', 'style' => 'margin-top: 10px')); ?>
				&nbsp;
				<?php echo CHtml::dropDownList('', '', Serverinfo::getCommands(), array('id' => 'commandlist', 'style' => 'margin-top: 10px')); ?>
				&nbsp;
				<?php echo CHtml::button('Отправить', array('id' => 'sendcommand', 'class' => 'btn btn-info')); ?>
			</td>
		</tr>
		<tr>
			<td>
				<pre id="output" style="min-height: 400px; font-size: 12px"></pre>
			</td>
		</tr>
		</tbody>
	</table>
<?php //endif; ?>