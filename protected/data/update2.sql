ALTER TABLE `amx_levels` ADD COLUMN `prefixes_edit` Enum( 'yes', 'no' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'no';
ALTER TABLE `amx_levels` ADD COLUMN `prefixes_view` Enum( 'yes', 'no' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'no';
ALTER TABLE `amx_levels` ADD COLUMN `rcon` Enum( 'yes', 'no' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'no';
