<?php
/**
 * Конфигурация приложения
 */

/**
 * @author Craft-Soft Team
 * @package CS:Bans
 * @version 1.0 beta
 * @copyright (C)2013 Craft-Soft.ru.  Все права защищены.
 * @link http://craft-soft.ru/
 * @license http://creativecommons.org/licenses/by-nc-sa/4.0/deed.ru  «Attribution-NonCommercial-ShareAlike»
 */

/**
 * Класс-заглушка, чтобы нормально использовать переменную $config из старого AmxBans
 */
class conf
{
	public $db_host = null;
	public $db_user = null;
	public $db_pass = null;
	public $db_db = null;
	public $db_prefix = null;
	public $robo_login = null;
	public $robo_pass1 = null;
	public $robo_pass2 = null;
	public $robo_testing = FALSE;
	public $code = NULL;
}
$config = new conf;

// Подключаем конфиг старого AmxBans
require_once ROOTPATH . '/include/db.config.inc.php';

// Главные параметры приложения
return array(
	'basePath'=>ROOTPATH . DIRECTORY_SEPARATOR . 'protected',
	'name'=>'Lviv CS Servers',
	'sourceLanguage' => 'ua',
	'language'=>'ua',

	// Предзагружаемые компоненты
	'preload'=>array(
		'log',
		'DConfig',
		),
	// Автозагружаемые модели и компоненты
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	// Компоненты приложения
	'components'=>array(
		'coreMessages'=>array(
			'basePath'=>null,
		),

		// Конфиг (из таблицы {{webconfig}})
		'config'=>array(
			'class' => 'DConfig'
		),

		// Подключение к БД
		'db'=>array(
			'connectionString' => 'mysql:host='.$config->db_host.';dbname='.$config->db_db,
			'emulatePrepare' => true,
			'username' => $config->db_user,
			'password' => $config->db_pass,
			'charset' => 'utf8',
			'tablePrefix'=>$config->db_prefix.'_',
			'autoConnect' => FALSE,
			'schemaCachingDuration' => 1000,
		),
		'cache'=>array(
			//'class'=>'system.caching.CDummyCache',
			'class'=>'system.caching.CFileCache',
		),
		// Обработка ошибок
		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),
		// Системный лог
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// Раскомментировать, если хотите, чтобы ошибки были выведены на страницах
				// array(
				//	'class'=>'CWebLogRoute',
				// ),
			),
		),
	),

	// Дополнительные параметры (вызываются так: Yii::app()->params['adminEmail'])
	'params'=>array(
		'adminEmail'=>'webmaster@example.com',
		'dbname' => $config->db_db,
		'Version' => '1.3',
	),
);
