<?php
/**
 * Motd
 */

/**
 * @author Craft-Soft Team
 * @package CS:Bans
 * @version 1.0 beta
 * @copyright (C)2013 Craft-Soft.ru.  Все права защищены.
 * @link http://craft-soft.ru/
 * @license http://creativecommons.org/licenses/by-nc-sa/4.0/deed.ru  «Attribution-NonCommercial-ShareAlike»
 */
 
 /*
 CREATE TABLE `amx_bans_cookies` (
	`id`  int UNSIGNED NULL AUTO_INCREMENT,
	`hash` VARCHAR(50) NULL,
	PRIMARY KEY (`id`)
) COLLATE='utf8_general_ci' ENGINE=InnoDB;

ALTER TABLE `amx_bans` ADD COLUMN `cookie_id` int UNSIGNED NULL;

ALTER TABLE `amx_bans` DROP COLUMN `imported`;

*/

define('ROOTPATH', dirname(__FILE__));

// Подключаем ядро
require_once(ROOTPATH . '/include/yii/framework/yii.php');
// Создаем приложение
Yii::createWebApplication(ROOTPATH . '/protected/config/motd.php')->run();