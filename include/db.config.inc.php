<?php
/**
 * Конфигурационные данные системы
 *
 * @author Craft-Soft Team
 * @version 1.0 beta
 * @copyright (C)2013 Craft-Soft.ru.  Все права защищены.
 * @package CS:Bans
 * @link http://craft-soft.ru/
*/

$config->db_host	= 'localhost';
$config->db_user	= 'root';
$config->db_pass	= '';
$config->db_db	    = 'hlds';
$config->db_prefix	= 'amx';
